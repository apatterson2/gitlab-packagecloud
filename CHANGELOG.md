# 0.2.3

Increase the rainbow worker processes from 4 to 8

# 0.2.2

Update license, use gitlab-vault and cleanup munin stuff

# 0.1.0

Initial release of gitlab-packagecloud
