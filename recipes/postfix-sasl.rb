include_recipe 'gitlab-vault'
gitlab_packagecloud_conf = GitLab::Vault.get(node, 'gitlab-packagecloud')

template '/etc/postfix/sasl_passwd' do
  mode '0600'
  variables gitlab_packagecloud_conf['packagecloud_rb'].to_hash
  notifies :run, 'execute[postmap hash:/etc/postfix/sasl_passwd]'
end

execute 'postmap hash:/etc/postfix/sasl_passwd' do
  # postmap creates /etc/postfix/sasl_passwd.db; that file should not be group-
  # or world-readable.
  umask '0077'
  action :nothing
end

file '/etc/postfix/sasl_passwd.db' do
  mode '0600'
end
