## Install yq
execute 'install yq via snap' do
  command 'snap install yq'
end

## Install the script
template '/etc/packagecloud/license_check.sh' do
  source 'license_check.sh.erb'
  owner 'root'
  group 'root'
  mode '0750'
  variables(
    snitch_url: node['gitlab-packagecloud']['snitch_url']
  )
  not_if { node['gitlab-packagecloud']['snitch_url'].nil? }
end

## Set up the CRON
cron 'check packagecloud license' do
  minute '0'
  hour '*/12'
  path '/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/snap/bin'
  command '/etc/packagecloud/license_check.sh'
  not_if { node['gitlab-packagecloud']['snitch_url'].nil? }
end
